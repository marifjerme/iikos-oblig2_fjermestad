#!/bin/bash
arr=($(pgrep chrome))
COUNTER=${#arr[*]}
TELLER=0

while [ "$TELLER" -lt "$COUNTER" ]; do
TALL=$(ps --no-headers -o maj_flt "${arr[${TELLER}]}")
if [[ "$TALL" -gt 1000 ]]; then
  echo "Chrome ${arr[${TELLER}]} har forårsaket  ${TALL} major page faults(mer enn 1000!)"
else
  echo "Chrome ${arr[${TELLER}]} har forårsaket  ${TALL} major page faults"
fi
let TELLER=TELLER+1
done


