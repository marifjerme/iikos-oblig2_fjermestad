#!/bin/bash
#Sjekker at man sender med ett faktiske argument
if [[ "$#" -ne 1 ]]; then
   echo "usage: $0 DIRECTORY"
   exit 0
fi

#Det som brukeren skriver inn som directory, passer på at det er backslash fornan og bak
DIR="${1}"

#Må bytte til det valgte directory, eller exit
cd ~/"${DIR}" || exit

#skriver ut informasjon on directory
temp=~/"${DIR}"
echo "Partisjonen /${DIR} befinner seg på er $(df -P "${temp}" | grep % | awk '{print $5}') full."

#Teller antall filer som ligger i directory
antall=$(find -- * | wc -l)
echo "Det finnes $antall filer."

#Finner den største filen
storrelse=$(find . -type f -printf "%s %p\n" | sort -nr | head -1 | awk '{print $1}')
fil=$(find . -type f -printf "%s %p\n" | sort -nr | head -1 | awk '{print $2}')
echo "Den største er $fil som er $storrelse stor."

#Total delt på antall filer er gjennomsnittet:
total=$(du -sb | cut -f 1)
gjen=$((total / antall))
echo "Gjennomsnittlig filstørrelse er ca $gjen bytes."

#Finne hardlinks til en fil, må finne den som er størst
echo "Filen $(find -- * -xdev -type f -printf "%n %p\n" | sort -nr | head -1 | awk '{print $2}') har flest hardlinks, den har $(find -- * -xdev -type f -printf "%n %p\n" | sort -nr | head -1 | awk '{print $1}')."

