#!/bin/bash
# no arguments, display a menu with option for info
# about processes

clear
while [ "$svar" != "9" ]
do
 echo ""
 echo "  1 - Hvem er jeg og hva er navnet på dette scriptet?"
 echo "  2 - Hvor lenge siden er det siden siste boot?"
 echo "  3 - Hva var gjennomsnittlig load siste minutt?"
 echo "  4 - Hvor mange prosesser og tråder finnes?"
 echo "  5 - Hvor mange context switcher fant sted siste sekund?"
 echo "  6 - Hvor mange interrupts fant sted siste sekund?"
 echo "  9 - Avslutt dette scriptet"
 echo ""
 echo -n "Velg en funksjon: "
 read -r svar
 echo ""
 case $svar in
  1)clear
    echo "Jeg er $(whoami) navnet på dette scriptet er $0"
    read -r
    clear
    ;;
 2)clear
    echo "Det er $(uptime -p | awk '{print $2,$3,$4,$5}') siden siste boot."
    read -r
    clear
    ;;
  3)clear
    echo "Gjennomsnittlig load siste minutt var $(uptime | awk '{print $9, $10, $11}')"
    read -r
    clear
    ;;
  4)clear
    echo "Det finnes $(grep processes /proc/stat | awk '{print $2}') prosseser og tråder."
    read -r
    clear
    ;;
  5)clear
    tall1=$(grep ctxt /proc/stat  | awk '{print $2}')
    sleep 1s
    tall2=$(grep ctxt /proc/stat | awk '{print $2}')
    res1=$((tall2-tall1))
    echo "Det var $res1 context switch'er som fant sted det siste sekundet."
    read -r
    clear
    ;;
  6)clear
    tall3=$(grep intr /proc/stat | awk '{print $2}')
    sleep 1s
    tall4=$(grep intr /proc/stat | awk '{print $2}')
    res=$((tall4-tall3))
    echo "Det var $res  interrupts det siste sekundet"
    read -r
    clear
    ;;
  9)echo Scriptet avsluttet
    exit
    ;;
  *)echo Ugyldig valg
    read -r
    clear
    ;;
 esac
done

