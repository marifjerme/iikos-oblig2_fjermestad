#!/bin/bash

array=( "$@" )
COUNTER=${#array[*]}
TELLER=0

while [ "$TELLER" -lt "$COUNTER" ]; do
PID="${array[${TELLER}]}"
FILNAVN="${PID}-$(date +%Y%m%d-%H:%M:%S).meminfo"
{
echo "******** Minne info om prosess med PID ${PID} ********"
echo "Total bruk av virtuelt minne (VmSize): $(grep VmSize /proc/1/status | awk '{print $2}')"
TALL1=$(grep VmData /proc/1/status | awk '{print $2}')
TALL2=$(grep VmStk /proc/1/status | awk '{print $2}')
TALL3=$(grep VmExe /proc/1/status | awk '{print $2}')
res=$((TALL1+TALL2+TALL3))
echo "Mengde privat virtuelt minne (VmData+VmStk+VmExe): $res "
echo "Mengde shared virtuelt minne (VmLib): $(grep VmLib /proc/1/status | awk '{print $2}')"
echo "Total bruk av fysisk minne (VmRSS): $(grep VmRSS /proc/1/status | awk '{print $2}')"
echo "Mengde fysisk minne som benyttes til page table (VmPTE): $(grep VmPTE /proc/1/status | awk '{print $2}')"
} > "$FILNAVN"
let TELLER=TELLER+1
done

